"""
MasterMind Game with Kivy

- Install dependencies:
    sudo pip install RPI.GPIO RandomWords
- The default password is 'help' or 'HELP'

- To enable mouse cursor on APP, open the ~/.kivy/config.ini and type this under the [module]
    [modules]
    touchring=scale=.3,alpha=.7,show_cursor=1

- To enable keyboard, open config.ini and set keyboard_mode as systemanddock

- Usage:
    To control GPIO, we must run APP with root permission.

        pi@raspberrypi ~ $ cd mastermind/
        pi@raspberrypi ~/mastermind $ sudo python main.py
    As you'll see, mouse cousor doesn't appear.
    To fix this, you need to make the same change to config.ini you made before,
    but to the root account's config file.
    Exit (Control+C) and copy over your home directory's Kivy configuration file to overwrite the root account's:
        pi@raspberrypi ~/mastermind $ sudo cp ~/.kivy/config.ini /root/.kivy/config.ini
    Run the example again and you'll be able to see mouse cursor

    If you cannot see anythin on your HDMI display, run like this:
	KIVY_BCM_DISPMANX_ID=2 python main.py


Designed by Wester de Weerdt     5/31/2016

"""

import glob
import time
import os
import datetime
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import NumericProperty, StringProperty, BooleanProperty, ListProperty
from kivy.uix.screenmanager import Screen
from kivy.config import Config
from kivy.uix.popup import Popup
from functools import partial
from kivy.clock import Clock
from random_words import RandomWords
from kivy.clock import Clock
from kivy.core.window import Window

debug = 0

if debug == 0:
    import RPi.GPIO as GPIO

password = ['HELP', 'help']


class CautionPopup(Popup):
    pass


class ConfirmPopup(Popup):
    pass


class MainScreen(Screen):
    fullscreen = BooleanProperty(False)

    def add_widget(self, *args):
        if 'content' in self.ids:
            return self.ids.content.add_widget(*args)
        return super(MainScreen, self).add_widget(*args)


class MainApp(App):

    # variables for GUI
    current_title = StringProperty()
    screen_names = []
    screens = {}
    hierarchy = []
    cur_screen = ""
    caution_popup = None
    confirm_popup = None

    confirm_param = None        # contains parameters for confirm_popup (callback string, numbers, etc)

    rw = None
    cur_word = ''
    word_list = []
    remaining = 0
    b_guess = True
    cnt = 0

    def build(self):
        """
        base function of kivy app
        :return:
        """
        if debug == 0:
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(21, GPIO.OUT)
            GPIO.output(21, False)
        else:
            Window.size = (1366, 768)

        Config.set('kivy', 'keyboard_mode', 'dock')
        self.title = 'Game'

        self.load_screen()

        self.caution_popup = CautionPopup()
        self.confirm_popup = ConfirmPopup()

        self.rw = RandomWords()
        if debug > 0:
            self.start_game('left')
        else:
            self.go_screen('welcome', 'right')

    def start_game(self, direction):
        screen = self.screens['game']
        # generate word list
        while len(self.cur_word) != 4:
            self.cur_word = self.rw.random_word()
        print self.cur_word

        self.gen_word()
        self.remaining = 3
        screen.ids['lb_remaining'].text = '[size=25]Remaining: ' + str(self.remaining) + '[/size]'
        self.go_screen('game', direction)

    def restart_game(self):
        tmp = ''
        while len(tmp) != 4 and tmp != self.cur_word:
            tmp = self.rw.random_word()
        self.cur_word = tmp
        print self.cur_word
        self.gen_word()

    def gen_word(self):
        screen = self.screens['game']
        self.word_list = []
        for i in range(10):
            tmp = ''

            while True:
                tmp = self.rw.random_word()
                if len(tmp) == 4 and tmp not in self.word_list:
                    equ_cnt = 0
                    for j in range(4):
                        if tmp[j] == self.cur_word[j]:
                            equ_cnt += 1
                    if equ_cnt > 0:
                        break

            self.word_list.append(tmp)
            screen.ids['btn_word_' + str(i)].text = tmp

    def btn_pressed(self, num):
        if self.remaining == 0 or not self.b_guess:
            self.confirm_popup.ids['lb_confirm'].text = "You cannot attempt no longer. Guess now?"
            self.confirm_param = ('guess', num)
            self.confirm_popup.open()

        else:
            screen = self.screens['game']
            match_cnt = 0
            for i in range(len(self.cur_word)):
                if self.cur_word[i] == self.word_list[num][i]:
                    match_cnt += 1
            self.remaining -= 1
            screen.ids['lb_remaining'].text = '[size=25]Remaining: ' + str(self.remaining) + '[/size]'
            screen.ids['lb_match'].text = '[size=25]Number of Matching Letters: ' + str(match_cnt) + '[/size]'

    def check_pwd(self):
        screen = self.screens['welcome']
        if screen.ids['txt_pwd'].text in password:
            self.start_game('left')
        else:
            self.caution_popup.ids['lb_content'].text = "Incorrect Password"
            self.caution_popup.open()

    def confirm_yes(self):
        """
        callback function of "Yes" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()
        if self.confirm_param:
            if self.confirm_param[0] == 'guess':
                self.go_screen('guess', 'left')
            elif self.confirm_param[0] == 'open_door':
                print "Opening door"
                if debug == 0:
                    GPIO.output(21, True)
                self.go_screen('finish', 'down')

    def guess(self):
        if self.b_guess:
            screen = self.screens['guess']
            val = screen.ids['txt_word'].text
            if val == self.cur_word:
                self.confirm_popup.ids['lb_confirm'].text = "Congratulations! You've passed!\n" \
                                                            "Do you want to open doors now?"
                self.confirm_param = ('open_door',)
                self.confirm_popup.open()
            else:
                self.remaining -= 1
                if self.remaining == 0:
                    self.caution_popup.ids['lb_content'].text = "You can no longer guess."
                    self.b_guess = False
                    self.cnt = 30
                    self.caution_popup.open()
                    Clock.schedule_interval(self.count_down, 1)

                else:
                    self.caution_popup.ids['lb_content'].text = "Does not match\n" \
                                                                "Please try again.(%d remaining)" % self.remaining
                    self.caution_popup.open()

    def count_down(self, *args):
        screen = self.screens['guess']
        if self.cnt > 0:
            self.cnt -= 1
            screen.ids['lb_count_down'].text = "Time left: %d" % self.cnt
        else:
            self.b_guess = True
            self.remaining = 3
            screen.ids['lb_count_down'].text = ""
            Clock.unschedule(self.count_down)

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        self.cur_screen = dest_screen
        screen = self.screens[dest_screen]
        sm = self.root.ids.sm

        self.current_title = screen.name

        if dest_screen == 'game':
            screen.ids['lb_remaining'].text = '[size=25]Remaining: ' + str(self.remaining) + '[/size]'
            screen.ids['lb_match'].text = '[size=25]Number of Matching Letters: [/size]'
        elif dest_screen == 'guess':
            self.remaining = 3

        sm.switch_to(screen, direction=direction)

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []
        if debug == 0:
            full_path_screens = glob.glob("/home/pi/mastermind/data/screens/*.kv")
        else:
            full_path_screens = glob.glob("data/screens/*.kv")
        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen
        return True


if __name__ == '__main__':

    app = MainApp()
    app.run()


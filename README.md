# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Mastermind game with Python Kivy Framework
* 

### How do I get set up? ###

* Summary of set up

* Configuration
    Open ~/.kivy/config.ini and insert below to enable mouse cursor.
        pi@raspberrypi ~ $ nano ~/.kivy/config.ini

        [modules]
        touchring=scale=.3,alpha=.7,show_cursor=1

* Dependencies
    
        sudo pip install RPI.GPIO RandomWords
* How to run tests

    To control GPIO, we must run APP with root permission.

        pi@raspberrypi ~ $ cd mastermind/
        pi@raspberrypi ~/mastermind $ sudo python main.py
    As you'll see, mouse cursor doesn't appear.
    To fix this, you need to make the same change to config.ini you made before,
    but to the root account's config file.
    Exit (Control+C) and copy over your home directory's Kivy configuration file to overwrite the root account's:

        pi@raspberrypi ~/mastermind $ sudo cp ~/.kivy/config.ini /root/.kivy/config.ini
    
    Run the example again and you'll be able to see mouse cursor

    If you cannot see anythin on your HDMI display, run like this:

        pi@raspberrypi ~/mastermind $ sudo KIVY_BCM_DISPMANX_ID=2 python main.py

    The default password is 'help' or 'HELP'